import React from "react";
import styled from 'styled-components';
 
const CalcWrapper = styled.div`
  margin-right: auto;
  margin-left: auto;
  width: 320px;
  background-color: #ecf0f1;
  box-shadow: 10px 10px 23px -12px rgba(0,0,0,0.75);
`;
export default CalcWrapper;