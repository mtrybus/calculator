import React from "react";
import styled from 'styled-components';
 
const Container = styled.div`
  padding: 3rem 1rem;
  margin-right: auto;
  margin-left: auto;
`;
export default Container;