import React from 'react';
import styled from 'styled-components';
 
const Header = styled.header`
  background-color: #2c3e50;
  padding: 30px 10px;
  text-align: center;
  margin-bottom: 70px;
  color: #fff;
`;
 
export default Header;