import React from 'react';
import styled from 'styled-components';

const StyledBtn = styled.div`
  padding: 10px;
  border: 1px solid #ddd;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #2c3e50;
  width: 80px;
  height: 80px;
  box-sizing: border-box;
  &:hover  {
    border-color: #aaa;
  }
`;

const CalcButton = (props) => {
  return(
    <StyledBtn onClick={() => props.numberChange(props.btnValue)}>
      {props.btnValue}
    </StyledBtn>
  )
}

export default CalcButton;