import React from "react";
import styled from 'styled-components';
 
const CalcAreaColumn = styled.div`
  display: flex; 
  flex-wrap: ${props => props.type == 'digit' ? 'wrap-reverse': 'wrap'};
  justify-content: ${props => props.type == 'operators' ? 'center': 'left'};
  width: ${props => props.type == 'operators' ? '80px': '100%'};
  align-items: center;
  background-color: ${props => props.type == 'operators' ? '#f7ab79': 'transparent'};
`;
export default CalcAreaColumn;