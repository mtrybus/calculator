import React from 'react';
import styled from 'styled-components';
 
const StyledScreen = styled.div`
  background-color: #2c3e50;
  color: #ecf0f1;
  padding: 2rem 1.5rem; 
  font-size: 2rem;
  text-align: right;
  overflow: auto;
`;

const CalcScreen = (props) => {
    return <StyledScreen>{props.value}</StyledScreen>;
}
export default CalcScreen;