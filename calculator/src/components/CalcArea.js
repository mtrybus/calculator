import React from "react";
import styled from 'styled-components';
 
const CalcArea = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
export default CalcArea;