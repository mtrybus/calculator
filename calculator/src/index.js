import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import {createStore} from 'redux';
import { Provider } from 'react-redux';

import './index.css';
import App from './containers/App';
import reducer from './store/reducer';

// const initialState = {
//     memoryValue: null,
//     value: '0',
//     operator: null,
//     waitingForOperand: false,
//     digitKeys: ['0', '.', '1', '2', '3', '4', '5', '6', '7', '8', '9'], 
//     operatorsKeys: ['+', '-', '*', '/'], 
//     finalKeys: ['=', 'C'], 
// }
// function reducer(state = initialState, action) {
//     switch (action.type) {
//         case 'INSERT_DOT': 
//         let value = state.value.indexOf('.') === -1 ? state.value + '.' : state.value;
//         if(state.waitingForOperand) value = '0.';
 
//         return {
//             ...state,
//             value,
//             waitingForOperand: false,
//         }
//         case 'INSERT_DIGIT': 
//         if(state.waitingForOperand){
//             return {
//                 ...state,
//                 value: action.payload,
//                 waitingForOperand: false,
//             }
//         }
//         return {
//             ...state,
//             value: state.value === '0' && state.value.indexOf('.') === -1 ? action.payload : state.value + action.payload
//         }
//         case 'INSERT_OPERATOR':
//         return {
//             ...state,
//             waitingForOperand: true,
//             operator: action.payload,
//             memoryValue: state.value, 
//         }

//         case 'CLEAR_DISPLAY':
//         return {
//             ...state,
//             value: '0',
//             waitingForOperand: false,
//             memoryValue: null,
//         }

//         case 'SUM_DIGITS':
//         const {memoryValue, operator} = state;
//         const nextValue = Number(state.value);

//         const operations = {
//             "/": (prevValue, nextValue) => prevValue / nextValue,
//             "*": (prevValue, nextValue) => prevValue * nextValue,
//             "+": (prevValue, nextValue) => prevValue + nextValue,
//             "-": (prevValue, nextValue) => prevValue - nextValue, 
//             "=": (prevValue, nextValue) => nextValue
//         }
//         if(!memoryValue) return { ...state }
        
//         const currentValue = memoryValue || 0;
//         let computedValue = operations[operator](Number(currentValue), Number(nextValue));
//         if(computedValue === Infinity ) computedValue = 0;
//         return {
//             ...state,
//             memoryValue: null,
//             value: String(computedValue),
//             waitingForOperand: true,
//         }

//         default:
//         return state
//     }
// }

const store = createStore(reducer); 

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
