import * as actionTypes from './actions';

const initialState = {
  memoryValue: null,
  value: '0',
  operator: null,
  waitingForOperand: false,
  digitKeys: ['0', '.', '1', '2', '3', '4', '5', '6', '7', '8', '9'], 
  operatorsKeys: ['+', '-', '*', '/'], 
  finalKeys: ['=', 'C'], 
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
      case actionTypes.INSERT_DOT: 
        let value = state.value.indexOf('.') === -1 ? state.value + '.' : state.value;
        if(state.waitingForOperand) value = '0.';
        return {
            ...state,
            value,
            waitingForOperand: false,
        }

      case actionTypes.INSERT_DIGIT: 
        if(state.waitingForOperand) {
            return {
                ...state,
                value: action.payload,
                waitingForOperand: false,
            }
        }
        return {
            ...state,
            value: state.value === '0' && state.value.indexOf('.') === -1 ? action.payload : state.value + action.payload
        }

      case actionTypes.INSERT_OPERATOR:
        return {
            ...state,
            waitingForOperand: true,
            operator: action.payload,
            memoryValue: state.value, 
        }

      case actionTypes.CLEAR_DISPLAY:
        return {
            ...state,
            value: '0',
            waitingForOperand: false,
            memoryValue: null,
        }

      case actionTypes.SUM_DIGITS:
        const {memoryValue, operator} = state;
        const nextValue = Number(state.value);

        const operations = {
            "/": (prevValue, nextValue) => prevValue / nextValue,
            "*": (prevValue, nextValue) => prevValue * nextValue,
            "+": (prevValue, nextValue) => prevValue + nextValue,
            "-": (prevValue, nextValue) => prevValue - nextValue, 
            "=": (prevValue, nextValue) => nextValue
        }
        if(!memoryValue) return { ...state }
      
        const currentValue = memoryValue || 0;
        let computedValue = operations[operator](Number(currentValue), Number(nextValue));
        if(computedValue === Infinity) computedValue = 0;
        return {
            ...state,
            memoryValue: null,
            value: String(computedValue),
            waitingForOperand: true,
        }

      default:
      return state
  }
}

export default reducer;
