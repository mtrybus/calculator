import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';

import  CalcButton from './../components/CalcButton';
import  CalcScreen from './../components/CalcScreen';
import  Container from './../components/Container';
import  CalcWrapper from './../components/CalcWrapper';
import  CalcArea from './../components/CalcArea';
import  CalcAreaColumn from './../components/CalcAreaColumn';
import  Header from './../components/Header';
import  Footer from './../components/Footer';
import * as actionTypes from './../store/actions';
 
class App extends Component {
  constructor(){
    console.log('zmianyyyyyyyyyyyyyyyyy')
    super();
  }
  handleClick(value){
    if(value === '.') {
      this.props.insertDot();
    } else {
      this.props.insertDigit(value)
    };
  }
  handleOperatorClick(value){ 
     this.props.insertOperator(value);
  }

  handleResultClick(value){ 
    value  === 'C' ? this.props.clearDisplay() : this.props.sumDigits();
  }

  render() {
    return (
      <Container>
        <Header>
          <span>Nawigacja</span>
        </Header>
        <CalcWrapper>
          <CalcScreen value={this.props.value}/>
           <CalcArea>
            <CalcAreaColumn type="digit">
              {this.props.digitKeys.map((item, key) => {
                return (
                  <CalcButton  key={key} btnValue={item} numberChange={this.handleClick.bind(this)} />
                )
              })}
            </CalcAreaColumn>

            <CalcAreaColumn type="operators">
              {this.props.operatorsKeys.map((item, key) => {
                return (
                  <CalcButton  key={key} btnValue={item} numberChange={this.handleOperatorClick.bind(this)} />
                )
              })}
            </CalcAreaColumn>
          </CalcArea> 

          <CalcAreaColumn type="final">
            {this.props.finalKeys.map((item, key) => {
              return (
                <CalcButton key={key} btnValue={item} numberChange={this.handleResultClick.bind(this)} />
              )
            })}
          </CalcAreaColumn>
        </CalcWrapper>
        <Footer>
          <span>Footer</span>
        </Footer>
      </Container>
    );
  }
}

const mapStateToProps = (state) => { 
  return {
    memoryValue: state.memoryValue,
    value: state.value,
    operator: state.operator,
    waitingForOperand: state.waitingForOperand,
    digitKeys: state.digitKeys,
    operatorsKeys: state.operatorsKeys,
    finalKeys: state.finalKeys,
  }
}
const mapDispatchToProps = dispatch => {
  return {
    insertDigit: (digit) => {
      dispatch({
        type: actionTypes.INSERT_DIGIT,
        payload: digit,
      })
    },
    insertDot: () => {
      dispatch({
        type: actionTypes.INSERT_DOT, 
      })
    },
    insertOperator: (operator) => {
      dispatch({
        type: actionTypes.INSERT_OPERATOR,
        payload: operator,
      })
    },
 
    clearDisplay: () => {
      dispatch({
        type: actionTypes.CLEAR_DISPLAY,
      })
    },
    sumDigits: () => {
      dispatch({
        type: actionTypes.SUM_DIGITS,
      })
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
 